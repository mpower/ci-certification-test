<?php
include 'inc/loader.php';

// Process Contact Form
if (isset($_GET['do'])) {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $email = strtolower(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL));
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['messages'] = '<div class="alert alert-danger text-center"><a href="#" class="close" data-dismiss="alert">&times;</a>Error: ' . $email . ' is not a valid email</div>';
        header("Location: /contact");
        die;
    }
    $post_subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_STRING);
    $subject = 'Contact Form Submission - ' . $post_subject;
    $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

    $message = '<p><strong>You have received a new message from:</strong> <a href="mailto:' . $email . '?subject=' . $post_subject . '">' . $name . ' &lt;' . $email . '&gt;</a></p><p>' . $message . '</p>';

    // Mail it
    $to = 'hello@tanukitech.cloud';
    $headers = 'MIME-Version: 1.0' . PHP_EOL;
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . PHP_EOL;
    $headers .= 'From: ' . $name . ' <' . $email . '>' . PHP_EOL;
    $headers .= 'X-Mailer: tanukitech.cloud' . PHP_EOL;
    $headers .= 'Reply-to: ' . $name . ' <' . $email . '>' . PHP_EOL;

    $messageHeader = "<html><head><title>$subject</title></head><body>";
    $messageFooter = '</body></html>';
    $message = $messageHeader . $message . $messageFooter;

    // Make it so
    mail($to, $subject, $message, $headers);

    // Display message
    $_SESSION['messages'] = '<div class="alert alert-success text-center"></a>Success: Message Sent</div>';
    header("Location: /");
    die;
} else {
    include 'inc/header.php';
    include 'inc/nav.php';
?>
    <section class="container page-start">
        <div class="row">
            <div class="col">
                <h1 class="page-header text-center">Contact Tanuki Tech</h1>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-8 mx-auto contact text-center">
                <form name="message" id="recaptcha-form" action="/contact?do" method="POST">
                    <div class="form-group">
                        <label for="name">Your Name:</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address:</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Message:</label>
                        <textarea rows="10" cols="100" class="form-control" name="message" id="message" placeholder="Message" required></textarea>
                    </div>
                    <button type="submit" class="margin-top-05 btn btn-outline-success"><i class="fas fa-paper-plane"></i> Send Message
                    </button>
                </form>
            </div>
        </div>
    </section>
<?php
    // Get footer
    include('inc/footer.php');
}
