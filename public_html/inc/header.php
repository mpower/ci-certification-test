<html lang="en-ca">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="handheldfriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
    <meta name="description" content="Corporate Website of Tanuki Tech">

    <!-- Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Tanuki Tech Demo Corporation">
    <meta property="og:title" content="Tanuki Tech">
    <meta property="og:description" content="Corporate Website of Tanuki Tech">
    <meta property="og:url" content="https://tanukitech.glcs.cloud">
    <meta property="og:image" content="">

    <title>Tanuki Tech Demo Corporation</title>

    <!-- CSS -->
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/default.css" rel="stylesheet">
    <link href="<?= (htmlspecialchars($_SERVER['SCRIPT_NAME']) === '/index.php') ? '../assets/css/cover.css' : NULL; ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Patua+One|Josefin+Sans:700" rel="stylesheet">
</head>

<body>
