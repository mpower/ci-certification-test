<!-- Nav -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" role="navigation">
    <div class="container">
        <a class="navbar-brand text-center" href="/"><strong>Tanuki Tech</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav-dropdown" aria-controls="navbar-nav-dropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-nav-dropdown">
            <ul class="navbar-nav ml-auto">
                <li class="<?= (htmlspecialchars($_SERVER['SCRIPT_NAME']) === '/index.php' && empty($_GET)) ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/"><i class="fas fa-home" aria-hidden="true"></i> Home</a>
                </li>
                <li class="<?= (htmlspecialchars($_SERVER['SCRIPT_NAME']) === '/news.php') ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/news"><i class="fas fa-newspaper" aria-hidden="true"></i>
                        News</a>
                </li>
                <li class="<?= (htmlspecialchars($_SERVER['SCRIPT_NAME']) === '/about.php') ? 'nav-item active' : 'nav-item'; ?>">
                    <a class="nav-link" href="/about"><i class="fas fa-info-circle" aria-hidden="true"></i>
                        About Us</a>
                </li>
                <li class="<?= (htmlspecialchars($_SERVER['SCRIPT_NAME']) === '/contact.php') ? 'nav-item active' : 'nav-item'; ?>"><a class="nav-link" href="/contact"><i class="fas fa-envelope" aria-hidden="true"></i>
                        Contact Us</a></li>
            </ul>
        </div>
    </div>
</nav>
<?php
// Messages
if (isset($_SESSION['messages'])) {
    echo '<div id="system-messages" class="container margin-top-40"><br><div class="col-md-8 col-12 mx-auto"><a href="#" class="close" data-dismiss="system-messages">&times;</a><strong>', $_SESSION['messages'], '</strong></div></div>';
    unset($_SESSION['messages']);
}
