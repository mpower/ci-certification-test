<!-- Footer -->
<!-- JS -->
<script type="text/javascript" src="../assets/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/font-awesome/js/all.min.js"></script>

<footer class="container-fluid margin-top-05">
    <div class="row">
        <div class="col-auto mx-auto">
            <p>Copyright &copy; <?= date("Y"); ?> <a href="https://www.maxtpower.com">Tanuki Tech Inc.</a></p>
        </div>
    </div>
</footer>

</body>

</html>