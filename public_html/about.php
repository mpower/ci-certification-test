<?php
include 'inc/loader.php';
include 'inc/header.php';
include 'inc/nav.php';
?>
<section class="container page-start">
    <div class="row">
        <div class="col">
            <h1 class="page-header text-center">About Us</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 mx-auto contact text-center">
            <p>Tanuki Tech is a GitLab Demo Corporation</p>
        </div>
    </div>
</section>
<?php
include 'inc/footer.php';
